from rest_framework import routers
from driver.viewsets import DriverViewSet

router = routers.DefaultRouter()
router.register(r'driver', DriverViewSet)