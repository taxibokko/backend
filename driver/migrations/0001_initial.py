# Generated by Django 2.2.2 on 2019-07-24 10:47

import datetime
from django.db import migrations, models
import phonenumber_field.modelfields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Driver',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('first_name', models.CharField(help_text='Entrez votre nom', max_length=30)),
                ('last_name', models.CharField(help_text='Entrez votre prenom', max_length=30)),
                ('phone_number', phonenumber_field.modelfields.PhoneNumberField(help_text='Entrez votre numero de telephone', max_length=128, region=None)),
                ('car_number', models.CharField(help_text='Matricule de la voiture', max_length=30)),
                ('driver_license', models.CharField(help_text='Numero de permis', max_length=30)),
                ('issue_date', models.DateField(default=datetime.date.today, help_text='date de delivrance')),
                ('expiration_date', models.DateField(default=datetime.date.today, help_text="date d'expiration")),
            ],
        ),
    ]
