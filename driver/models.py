from django.db import models
from phonenumber_field.modelfields import PhoneNumberField
from datetime import date
from django.utils import timezone

# Create your models here.

class Driver(models.Model):
    first_name = models.CharField(max_length=30,help_text="Entrez votre nom")
    last_name = models.CharField(max_length=30,help_text="Entrez votre prenom")
    phone_number = PhoneNumberField(help_text="Entrez votre numero de telephone")    
    car_number = models.CharField(max_length=30,help_text="Matricule de la voiture")
    driver_license = models.CharField(max_length=30,help_text="Numero de permis")
    issue_date = models.DateField(default=date.today,help_text="date de delivrance")
    expiration_date = models.DateField(default=date.today,help_text="date d'expiration")

