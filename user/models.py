from django.db import models
from phonenumber_field.modelfields import PhoneNumberField
# Create your models here.

class Client(models.Model):
    name = models.CharField(max_length=30, help_text="Entrez votre nom")
    password = models.CharField(max_length=30, help_text="Entrez votre mot de passe")
    phone = PhoneNumberField(blank=True, help_text='Contact phone number')